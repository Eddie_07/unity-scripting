﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleRotation : MonoBehaviour
{
    [SerializeField]
    private Vector3 rotationAxis;
    public float rotationSpeed;

    // Update is called once per frame
    void Update()
    {
        rotationAxis = CapsuleMovement.ClampVector3(rotationAxis);

        transform.Rotate((Time.deltaTime * rotationSpeed) * rotationAxis);
    }

    
}
